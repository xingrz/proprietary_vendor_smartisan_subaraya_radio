DEVICE := surabaya

MODEM_IMAGE := firmware-update/NON-HLOS.bin
XBL_ELF := firmware-update/xbl.elf
APPSBOOT_MBN := firmware-update/emmc_appsboot.mbn

TIMESTAMP := $(shell strings $(MODEM_IMAGE) | sed -n 's/.*"Time_Stamp": "\([^"]*\)"/\1/p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's/[ :-]*//g')

HASH_XBL := $(shell openssl dgst -r -sha1 $(XBL_ELF) | cut -d ' ' -f 1)
HASH_APPSBOOT := $(shell openssl dgst -r -sha1 $(APPSBOOT_MBN) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(XBL_ELF) $(APPSBOOT_MBN)
ifneq ($(HASH_XBL), a6a61f77999121270556169752516701758f0c64)
	$(error SHA-1 of xbl.elf mismatch)
endif
ifneq ($(HASH_APPSBOOT), 46b93c0ffdae84881d098ac0ff790b92ec3d33b1)
	$(error SHA-1 of emmc_appsboot.mbn mismatch)
endif

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMAGE)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
